<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp_chef');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost:80');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'rAC_NIohUPp8t?zC3;Y 7V}vzm JT-AAbq?!|`)rI,*CmcD-IXm)<k`<<@:r I+f');
define('SECURE_AUTH_KEY',  '6MZk_@pA (LvMf|EUX|{GypsKJx.WR!GKde217rYA8Fx[V}vQ/[1o-al#xa!Eq?/');
define('LOGGED_IN_KEY',    '?(ou@MoF28GA|n>:^(56Aa>l>r,nZm~3y4`3)_y=u[68=^^.HGyTyj#3+C;8d[G%');
define('NONCE_KEY',        'B>7D+ee{v~+|IyX$aNfh+1|Xh|$~}}0=R!;kUei-hf2|UY)5Q-k~TDit6=,h43m~');
define('AUTH_SALT',        'I4PF:~1CZ;3g=0c]rV2pT(di}9ckJLaA+W%F{vn{A#{4~yyDK5V$ONTU,Xz;GZ+E');
define('SECURE_AUTH_SALT', '>i&[-WQ:SE1!EYc<_^n1oHrN_iv- Gb)zZ9=.6nKKdnS.o 2;S+5z7t*_S6|vKtb');
define('LOGGED_IN_SALT',   '$KJ<q$t/~-Br>)5P/l6yF# 0(Z}/Fx#@Q% 2)Dow=BKXp~CpmJZ`J8k3(KB,LC,z');
define('NONCE_SALT',       'Ld|I~%X[Q[Q*cIFXv(be[`/tES)w&9 =%0^53Ze8[u(sD!C-_/h6|GW$lon?zJ]^');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wpWPC_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
